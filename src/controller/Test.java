package controller;

import interfaces.Taxable;

import java.util.ArrayList;

import model.BankAccount;
import model.CompanyTaxable;
import model.Country;
import model.Data;
import model.Person;
import model.PersonTaxable;
import model.ProductTaxable;
import model.TaxCalculator;
import interfaces.Measurable;

public class Test {
	
	public static void main(String[] args) {
		Test test = new Test();
		test.testCase();
		test.testTax();
	}
	
	public void testCase() {
		Measurable[] minimize = new Measurable[3];
		
		BankAccount b1 = new BankAccount(5500);
		BankAccount b2 = new BankAccount(7000);
		Country c1 = new Country(120);
		Country c2 = new Country(350);
		Person p1 = new Person("Robin",182);
		Person p2 = new Person("Robert",178);
		
		minimize[0] = Data.min(b1, b2);
		minimize[1] = Data.min(c1, c2);
		minimize[2] = Data.min(p1, p2);
		
		System.out.println("Minimize of BankAccount: "+Data.min(b1, b2));
		System.out.println("Minimize of Country: "+Data.min(c1, c2));
		System.out.println("Minimize of Persons: "+Data.min(p1, p2));		
		System.out.println("Average of BankAccount: "+Data.average(minimize)+"\n");
		
	}
	
	public void testTax() {

		ArrayList<Taxable> personList = new ArrayList<Taxable>();
		ArrayList<Taxable> companyList = new ArrayList<Taxable>();
		ArrayList<Taxable> productList = new ArrayList<Taxable>();
		ArrayList<Taxable> totalList = new ArrayList<Taxable>();
		
		TaxCalculator tax = new TaxCalculator();
		
		PersonTaxable p1 = new PersonTaxable("Robin",75000);
		PersonTaxable p2 = new PersonTaxable("Robert",550000);
		PersonTaxable p3 = new PersonTaxable("Roberto",33000);
		
		CompanyTaxable com1 = new CompanyTaxable("Avenue Co.,Ltd.",750000,350000);
		CompanyTaxable com2 = new CompanyTaxable("HeadHunter Co.,Ltd.",1000000,75000);
		CompanyTaxable com3 = new CompanyTaxable("BoszBistro Ltd.",450000,150000);
		
		ProductTaxable pro1 = new ProductTaxable("Computer",55000);
		ProductTaxable pro2 = new ProductTaxable("Mobile",23000);
		ProductTaxable pro3 = new ProductTaxable("Television",15000);
		
		personList.add(p1);
		personList.add(p2);
		personList.add(p3);
		
		companyList.add(com1);
		companyList.add(com2);
		companyList.add(com3);
		
		productList.add(pro1);
		productList.add(pro2);
		productList.add(pro3);
		
		totalList.add(p1);
		totalList.add(p2);
		totalList.add(p3);
		totalList.add(com1);
		totalList.add(com2);
		totalList.add(com3);
		totalList.add(pro1);
		totalList.add(pro2);
		totalList.add(pro3);
		
		System.out.println("Taxable of "+p1.getName()+" ,"+p2.getName()+" ,"+p3.getName()+": "
							+tax.sum(personList));
		
		System.out.println("Taxable of "+com1.getCompanyName()+" ,"+com2.getCompanyName()+" ,"
							+com3.getCompanyName()+": "+tax.sum(companyList));
		
		System.out.println("Taxable of "+pro1.getProName()+" ,"+pro2.getProName()+" ,"
							+pro3.getProName()+": "+tax.sum(productList));
		
		System.out.println("Total summation of taxable: "+tax.sum(totalList));
	}

}
