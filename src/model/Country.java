package model;

import interfaces.Measurable;

public class Country implements Measurable {

	private double area;
	
	public Country(double area) {
		this.area = area;
		
	}
	
	@Override
	public double getMeasure() {
		return area;
		
	}
	
	public String toString() {
		return area+"";
	}

}
