package model;

import java.util.ArrayList;
import interfaces.Taxable;

public class TaxCalculator {
	
	public static double sum(ArrayList<Taxable> arrayTax) {
			
			double sum = 0;
			
			for (Taxable a: arrayTax) {
				sum = sum+ a.getTax();
			}
			
			return sum;
		}

}
