package model;

import interfaces.Taxable;

public class ProductTaxable implements Taxable {
	
	private String productName;
	private double price;
	
	public ProductTaxable(String proName, double price) {
		this.productName = proName;
		this.price = price;
	}

	public String getProName(){
		return productName;
	}
	
	@Override
	public double getTax() {
		return price*0.07;
	}

}
