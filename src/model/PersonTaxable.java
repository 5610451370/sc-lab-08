package model;

import interfaces.Taxable;

public class PersonTaxable implements Taxable {
	
	private String name;
	private double salary;
	
	public PersonTaxable(String name, double salary) {
		this.name = name;
		this.salary = salary;
	}
	
	public String getName() {
		return name;
	}

	@Override
	public double getTax() {
		
		if (salary <=300000) return salary*0.05;
		
		else return (salary*0.05)+((salary-300000)*0.1);
		
	}

}
