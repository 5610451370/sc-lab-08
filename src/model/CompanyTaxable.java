package model;

import interfaces.Taxable;

public class CompanyTaxable implements Taxable {
	
	private String companyName;
	private double receipts;
	private double disbursement;

	public CompanyTaxable(String comName, double receipts, double disbursement) {
		this.companyName = comName;
		this.receipts = receipts;
		this.disbursement = disbursement;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	
	@Override
	public double getTax() {
		return ((receipts-disbursement)*0.3);
	}

}
