package model;

import interfaces.Measurable;

public class Person implements Measurable {
	
	private String name;
	private double height;
	
	public Person(String name, double height) {
		this.name=name;
		this.height=height;
	}
	
	@Override
	public double getMeasure() {

		return this.height;
	}
	
	public String toString() {
		return name+"  "+height;
	}

}
