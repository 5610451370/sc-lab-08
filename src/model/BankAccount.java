package model;

import interfaces.Measurable;

public class BankAccount implements Measurable {
	
	private double balance;
	
	public BankAccount(double balance) {
		this.balance = balance;
		
	}
	
	@Override
	public double getMeasure() {
		return balance;
		
	}
	
	public String toString() {
		return balance+"";
	}

}
